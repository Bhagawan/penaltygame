# Penalty Game
Angry Birds -like game.  
Made with LibGDX, (Ashley, Box2d and Scene2d)  
Also contains custom splash screen and OneSignal connection. (retrofit)

<img src="https://gitlab.com/Bhagawan/penaltygame/-/raw/master/menu.png" width="480" height ="272">
<img src="https://gitlab.com/Bhagawan/penaltygame/-/raw/master/game.png" width="480" height ="272">
