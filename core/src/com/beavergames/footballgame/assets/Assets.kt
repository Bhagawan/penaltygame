package com.beavergames.footballgame.assets

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.ui.Skin

class Assets {
    companion object {
        var assetManager = AssetManager()
        lateinit var animKick : Animation<TextureRegion>


        fun init() {
            assetManager = AssetManager()
            assetManager.load("skin/skin.atlas", TextureAtlas::class.java)
            assetManager.load("skin/skin.json", Skin::class.java)

            while(!assetManager.update()) { println("Loading assets") }
            loadAnimations()
        }

        fun dispose() {
            assetManager.dispose()
        }

        private fun loadAnimations() {
            animKick = createAnim("kick_anim", 92, 202, 0.5f)
        }

        private fun createAnim(stripName: String, frameWidth : Int, frameHeight: Int, duration: Float) : Animation<TextureRegion> {
            val animStrip = assetManager.get("skin/skin.json", Skin::class.java)
                .getRegion(stripName)
                .split(frameWidth, frameHeight)
            val animFrames =
                com.badlogic.gdx.utils.Array(Array<TextureRegion>(animStrip[0].size * animStrip.size) { t -> animStrip[t / animStrip[0].size][t % animStrip[0].size] })
            return Animation(duration, animFrames, Animation.PlayMode.LOOP)
        }
    }
}