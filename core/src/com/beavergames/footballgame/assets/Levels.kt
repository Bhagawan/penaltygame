package com.beavergames.footballgame.assets

import com.beavergames.footballgame.util.PlayerPosition

class Levels {
    companion object {
        private val levels = ArrayList<List<PlayerPosition>>()

        init {
            levels.add(listOf(
                PlayerPosition(-3.0f, 0.0f, 0.0f)
            ))
            levels.add(listOf(
                PlayerPosition(-7.0f, 0.0f, 0.0f),
                PlayerPosition(-7.0f, 3.0f, 0.0f),
                PlayerPosition(-7.0f, 6.0f, 0.0f)
            ))
            levels.add(listOf(
                PlayerPosition(-5.0f, 0.0f, 0.0f),
                PlayerPosition(-5.0f, 3.0f, 0.0f),
                PlayerPosition(-5.0f, 6.0f, 0.0f),
                PlayerPosition(-8.0f, 0.0f, 0.0f),
                PlayerPosition(-8.0f, 3.0f, 0.0f),
                PlayerPosition(-8.0f, 6.0f, 0.0f)
            ))
            levels.add(listOf(
                PlayerPosition(-5.0f, 0.0f, 0.0f),
                PlayerPosition(-7.0f, 0.0f, 0.0f),
                PlayerPosition(-4.0f, 3.0f, -90.0f),
                PlayerPosition(-5.0f, 4.0f, 0.0f),
                PlayerPosition(-7.0f, 4.0f, 0.0f),
                PlayerPosition(-4.0f, 7.0f, -90.0f),
                PlayerPosition(-5.0f, 8.0f, 0.0f),
                PlayerPosition(-7.0f, 8.0f, 0.0f),
                PlayerPosition(-4.0f, 11.0f, -90.0f)
            ))
            levels.add(listOf(
                PlayerPosition(-4.0f, 0.0f, 0.0f),
                PlayerPosition(-7.0f, 0.0f, 0.0f),
                PlayerPosition(-10.0f, 0.0f, 0.0f),
                PlayerPosition(-3.5f, 3.0f, -90.0f),
                PlayerPosition(-6.5f, 3.0f, -90.0f),
                PlayerPosition(-6.0f, 4.0f, 0.0f),
                PlayerPosition(-8.0f, 6.0f, 0.0f),
                PlayerPosition(-5.5f, 7.0f, -90.0f)
            ))
            levels.add(listOf(
                PlayerPosition(-6.0f, 0.0f, -90.0f),
                PlayerPosition(-6.0f, 1.0f, -90.0f),
                PlayerPosition(-6.0f, 2.0f, -90.0f),
                PlayerPosition(-6.0f, 3.0f, -90.0f),
                PlayerPosition(-6.0f, 4.0f, -90.0f),
                PlayerPosition(-6.0f, 5.0f, -90.0f),
                PlayerPosition(-6.0f, 6.0f, -90.0f),
                PlayerPosition(-6.0f, 7.0f, -90.0f)
            ))
            levels.add(listOf(
                PlayerPosition(-6.0f, 0.0f, 0.0f),
                PlayerPosition(-8.0f, 0.0f, 0.0f),
                PlayerPosition(-10.0f, 0.0f, 0.0f),
                PlayerPosition(-12.0f, 0.0f, 0.0f),
                PlayerPosition(-5.0f, 4.0f, -90.0f),
                PlayerPosition(-9.0f, 4.0f, -90.0f),
                PlayerPosition(-7.0f, 5.0f, -90.0f),
                PlayerPosition(-7.0f, 9.0f, -90.0f),
                PlayerPosition(-8.0f, 5.0f, 0.0f),
                PlayerPosition(-10.0f, 5.0f, 0.0f),
                PlayerPosition(-9.0f, 9.0f, 0.0f)
            ))
            levels.add(listOf(
                PlayerPosition(-5.0f, 0.0f, 0.0f),
                PlayerPosition(-8.0f, 0.0f, 0.0f),
                PlayerPosition(-11.0f, 0.0f, 0.0f),
                PlayerPosition(-14.0f, 0.0f, 0.0f),
                PlayerPosition(-4.5f, 3.0f, -90.0f),
                PlayerPosition(-7.5f, 3.0f, -90.0f),
                PlayerPosition(-10.5f, 3.0f, -90.0f),
                PlayerPosition(-6.0f, 4.0f, 0.0f),
                PlayerPosition(-9.0f, 4.0f, 0.0f),
                PlayerPosition(-10.0f, 4.0f, 0.0f),
                PlayerPosition(-13.0f, 4.0f, 0.0f),
                PlayerPosition(-5.5f, 7.0f, -90.0f),
                PlayerPosition(-8.5f, 7.0f, -90.0f),
                PlayerPosition(-8.0f, 8.0f, 0.0f),
                PlayerPosition(-11.0f, 8.0f, 0.0f),
                PlayerPosition(-7.5f, 11.0f, -90.0f)
            ))
            levels.add(listOf(
                PlayerPosition(-5.0f, 0.0f, 0.0f),
                PlayerPosition(-6.0f, 0.0f, 0.0f),
                PlayerPosition(-8.0f, 0.0f, 0.0f),
                PlayerPosition(-9.0f, 0.0f, 0.0f),
                PlayerPosition(-5.0f, 4.0f, -90.0f),
                PlayerPosition(-5.0f, 3.0f, 0.0f),
                PlayerPosition(-9.0f, 3.0f, 0.0f),
                PlayerPosition(-7.0f, 4.0f, 0.0f),
                PlayerPosition(-3.0f, 6.0f, -90.0f),
                PlayerPosition(-7.0f, 6.0f, -90.0f),
                PlayerPosition(-7.0f, 7.0f, 0.0f)
            ))
            levels.add(listOf(
                PlayerPosition(-5.0f, 0.0f, 0.0f),
                PlayerPosition(-6.0f, 0.0f, 0.0f),
                PlayerPosition(-8.0f, 0.0f, 0.0f),
                PlayerPosition(-10.0f, 0.0f, 0.0f),
                PlayerPosition(-12.0f, 0.0f, 0.0f),
                PlayerPosition(-12.0f, 3.0f, 0.0f),
                PlayerPosition(-12.0f, 6.0f, 0.0f),
                PlayerPosition(-12.0f, 9.0f, 0.0f),
                PlayerPosition(-4.0f, 3.0f, -90.0f),
                PlayerPosition(-7.0f, 3.0f, -90.0f),
                PlayerPosition(-4.0f, 7.0f, -90.0f),
                PlayerPosition(-5.0f, 4.0f, 0.0f),
                PlayerPosition(-6.0f, 4.0f, 0.0f),
                PlayerPosition(-8.0f, 4.0f, 0.0f),
            ))
            levels.add(listOf(
                PlayerPosition(-4.0f, 0.0f, 0.0f),
                PlayerPosition(-6.0f, 0.0f, 0.0f),
                PlayerPosition(-8.0f, 0.0f, 0.0f),
                PlayerPosition(-4.0f, 3.0f, 0.0f),
                PlayerPosition(-6.0f, 3.0f, 0.0f),
                PlayerPosition(-8.0f, 3.0f, 0.0f),
                PlayerPosition(-5.0f, 7.0f, 0.0f),
                PlayerPosition(-7.0f, 7.0f, 0.0f),
                PlayerPosition(-3.5f, 6.0f, -90.0f),
                PlayerPosition(-5.5f, 6.0f, -90.0f)
            ))
        }

        fun getLevels() : ArrayList<List<PlayerPosition>> = levels
    }
}