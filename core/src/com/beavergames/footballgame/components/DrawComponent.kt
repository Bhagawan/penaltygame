package com.beavergames.footballgame.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable

class DrawComponent(): Component {
    var textureRegion : TextureRegion? = null
    var tiledDrawable : TiledDrawable? = null


    constructor(textureRegion: TextureRegion) : this() {
        this.textureRegion = textureRegion
    }

    constructor(drawable: TiledDrawable) : this() {
        this.tiledDrawable = drawable
    }
}