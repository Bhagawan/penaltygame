package com.beavergames.footballgame.systems

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.Array
import com.beavergames.footballgame.components.BallComponent
import com.beavergames.footballgame.components.TransformComponent

class BodyTextureSystem(private val world :World, priority: Int) : EntitySystem(priority) {
    private val terrainHeight = Gdx.graphics.height / 10.0f
    private val one = (Gdx.graphics.height - terrainHeight / 10.0f) / 10.0f

    override fun update(deltaTime: Float) {
        val bodies : Array<Body> = Array()
        world.getBodies(bodies)
        for(body in bodies) {
            val entity = body.userData
            if(entity is Entity) {
                entity.getComponent(TransformComponent::class.java)?.let {
                    var dX = -0.5f * one
                    var dY = -0.5f * one
                    if(entity.getComponent(BallComponent::class.java) == null) {
                        dX = when(it.initialAngle) {
                            -90.0f -> 1.65f * one
                            else -> -0.6f * one
                        }
                        dY = when(it.initialAngle) {
                            -90.0f -> -0.6f * one
                            else -> -1.65f * one
                        }
                    }
                    it.pos.x = body.position.x * one + dX
                    it.pos.y = body.position.y * one + dY
                    it.angle = MathUtils.radiansToDegrees * body.angle + it.initialAngle
                }
            }
        }
    }
}