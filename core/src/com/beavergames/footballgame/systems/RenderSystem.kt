package com.beavergames.footballgame.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.Array
import com.beavergames.footballgame.components.AnimationComponent
import com.beavergames.footballgame.components.DrawComponent
import com.beavergames.footballgame.components.TransformComponent
import kotlin.math.sign

class RenderSystem(private val bath: SpriteBatch, private val camera: Camera, priority: Int):
    IteratingSystem(Family.all(DrawComponent::class.java, TransformComponent::class.java).get(), priority) {

    private val dM = ComponentMapper.getFor(DrawComponent::class.java)
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)
    private val aM = ComponentMapper.getFor(AnimationComponent::class.java)

    private val queue = Array<Entity>()

    private val comparator =
        Comparator<Entity> { p0, p1 -> (tM.get(p0).pos.z - tM.get(p1).pos.z).sign.toInt() }

    override fun update(deltaTime: Float) {
        super.update(deltaTime)
        camera.update()
        bath.projectionMatrix = camera.combined

        queue.sort(comparator)

        var dC: DrawComponent?
        var tC: TransformComponent?
        var aC: AnimationComponent?

        bath.begin()
        for (entity in queue) {
            dC = dM.get(entity)
            tC = tM.get(entity)
            aC = aM.get(entity)
            aC?.let {
                bath.draw(aC.animation.getKeyFrame(aC.currentPosition), tC.pos.x, tC.pos.y, tC.originX, tC.originY,
                    tC.width , tC.height, tC.scale, tC.scale, tC.angle)
                if(aC.currentPosition < aC.animation.animationDuration) aC.currentPosition += deltaTime
            } ?: run {
                dC.tiledDrawable?.draw(bath, tC.pos.x, tC.pos.y, tC.width, tC.height)
                dC.textureRegion?.let {
                    bath.draw(
                        it, tC.pos.x, tC.pos.y, tC.originX, tC.originY,
                        tC.width, tC.height, tC.scale, tC.scale, tC.angle)
                }
            }
        }
        bath.end()
        queue.clear()
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        entity?.let { queue.add(it) }
    }
}