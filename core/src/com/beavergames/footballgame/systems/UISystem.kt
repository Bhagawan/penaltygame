package com.beavergames.footballgame.systems

import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.beavergames.footballgame.assets.Assets
import com.beavergames.footballgame.assets.Levels
import com.beavergames.footballgame.util.LevelActor
import com.beavergames.footballgame.util.PlayerPosition
import com.beavergames.footballgame.util.RecordWidget

class UISystem(level: List<PlayerPosition>, priority: Int): EntitySystem(priority) {
    private val stage = Stage()
    private var ballsResult = Array<Boolean?>(5) { null }
    private val rWidget = RecordWidget(ballsResult)
    private val menuDialog = createMenuDialog()

    private var uiInterface: UIInterface? = null
    private val levelIndex = Levels.getLevels().indexOf(level)
    private var nextLevel: List<PlayerPosition>? = null


    init {
        createUI()
        getNextLevel(level)
    }

    override fun update(deltaTime: Float) {
        stage.act(deltaTime)
        stage.draw()
    }

    //// Public

    fun setInterface(i: UIInterface) {
        uiInterface = i
    }

    fun hit(goal: Boolean) {
        for(n in 4 downTo 0) {
            if(ballsResult[n] == null) {
                ballsResult[n] = goal
                break
            }
        }
        rWidget.setResult(ballsResult)
        if(ballsResult[0] != null) {
            uiInterface?.pause(true)
            uiInterface?.saveRecord(ballsResult)
            showEndDialog()
        }
    }

    fun getStage(): Stage = stage


    //// Private

    private fun createUI() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val table = Table()
        table.setFillParent(true)
        stage.addActor(table)
        table.top()

        val size = Gdx.graphics.height / 10.0f

        val settingsButton = ImageButton(skin.getDrawable("gear")).apply {
            addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    uiInterface?.pause(true)
                    menuDialog.show(stage)
                }
            })
        }
        table.add(settingsButton).left().top().size(size * 1.3f, size * 1.3f)

        table.add(rWidget).top().expandX().height(size).padLeft(20.0f).padRight(20.0f).width(size * 5 + size / 10 * 4)
        table.add().width(size)

    }

    private fun createMenuDialog() : Dialog {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val menu = Dialog("", skin)
        menu.setSize(stage.width / 3.0f, stage.height / 3.0f)
        menu.contentTable.pad(50.0f)
        menu.contentTable.add(Label("Пауза", skin, "header")).center().expand().colspan(3)
        menu.contentTable.row()

        val exitButton = ImageButton(SpriteDrawable(skin.getSprite("exit").apply { setSize(Gdx.graphics.height / 6.0f, Gdx.graphics.height / 6.0f) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    uiInterface?.exit()
                }
            })
        }
        menu.contentTable.add(exitButton).center().bottom().expandX().size(Gdx.graphics.height / 8.0f, Gdx.graphics.height / 8.0f).pad(20.0f)

        val restartButton = ImageButton(SpriteDrawable(skin.getSprite("retry").apply { setSize(Gdx.graphics.height / 6.0f, Gdx.graphics.height / 6.0f) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    uiInterface?.restart()
                    for(n in 0..4) ballsResult[n] = null
                    rWidget.setResult(ballsResult)
                    menu.hide()
                }
            })
        }
        menu.contentTable.add(restartButton).center().bottom().expandX().size(Gdx.graphics.height / 8.0f, Gdx.graphics.height / 8.0f).pad(20.0f)

        val continueButton = ImageButton(SpriteDrawable(skin.getSprite("play").apply { setSize(Gdx.graphics.height / 6.0f, Gdx.graphics.height / 6.0f) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    menu.hide()
                    uiInterface?.pause(false)
                }
            })
        }
        menu.contentTable.add(continueButton).center().bottom().expandX().size(Gdx.graphics.height / 8.0f, Gdx.graphics.height / 8.0f).pad(20.0f)

        return menu
    }

    private fun showEndDialog() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val endDialog = Dialog("", skin)
        rWidget.hide()

        endDialog.contentTable.pad(10.0f)
        endDialog.contentTable.add(Label("Игра завершена!", skin, "recordsHeader")).expandX().center().colspan(if(nextLevel == null) 2 else 3)
        endDialog.contentTable.row()

        val size = Gdx.graphics.height / 9.0f

        val result = LevelActor(levelIndex, ballsResult, stage.width / 2.0f * 0.75f, stage.width / 2.0f * 0.75f * (80 / 50))
        endDialog.contentTable.add(result).fillX().height(size).colspan(if(nextLevel == null) 2 else 3)
            .minHeight(stage.width / 2.0f * 0.75f * (80 / 50))
            .minWidth(stage.width / 2.0f * 0.75f)
        endDialog.contentTable.row()

        val exitButton = ImageButton(SpriteDrawable(skin.getSprite("exit").apply { setSize(size, size) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    uiInterface?.exit()
                }
            })
        }
        endDialog.contentTable.add(exitButton).center().bottom().expandX().size(stage.width / 15.0f, stage.width / 15.0f)

        val restartButton = ImageButton(SpriteDrawable(skin.getSprite("retry").apply { setSize(size, size) })).also {
            it.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    uiInterface?.restart()
                    for(n in 0..4) ballsResult[n] = null
                    rWidget.setResult(ballsResult)
                    endDialog.hide()
                    rWidget.show()
                }
            })
        }
        endDialog.contentTable.add(restartButton).center().bottom().expandX().size(stage.width / 15.0f, stage.width / 15.0f)

        if(nextLevel != null) {
            val nextLevelButton = ImageButton(SpriteDrawable(skin.getSprite("next").apply { setSize(size, size) })).also {
                it.addListener(object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        nextLevel?.let {level -> uiInterface?.nextLevel(level) }
                    }
                })
            }
            endDialog.contentTable.add(nextLevelButton).center().bottom().expandX().size(stage.width / 15.0f, stage.width / 15.0f)
        }
        endDialog.show(stage)
    }

    private fun getNextLevel(level: List<PlayerPosition>) {
        val levels = Levels.getLevels()
        for(n in levels.indices) {
            if(levels[n] == level && n < levels.size - 1) {
                nextLevel = levels[n+1]
                break
            }
        }
    }


    interface UIInterface {
        fun pause(pause: Boolean)
        fun exit()
        fun restart()
        fun nextLevel(level: List<PlayerPosition>)
        fun saveRecord(result: Array<Boolean?>)
    }
}