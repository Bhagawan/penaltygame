package com.beavergames.footballgame.systems

import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable
import com.beavergames.footballgame.assets.Assets

class BackgroundSystem(private val camera: Camera, priority: Int) : EntitySystem(priority) {
    private val seatTile = TiledDrawable(Assets.assetManager.get("skin/skin.json", Skin::class.java).getSprite("seatTile"))

    private val batch =  SpriteBatch()

    override fun update(deltaTime: Float) {
        batch.projectionMatrix = camera.combined
        batch.begin()

        val x : Int = (camera.position.x - camera.viewportWidth / 2.0f).toInt() / seatTile.region.regionWidth
        val y : Int = (camera.position.y - camera.viewportHeight / 2.0f).toInt() / seatTile.region.regionHeight
        seatTile.draw(batch, x.toFloat(), y.toFloat(), camera.position.x + camera.viewportWidth / 2 - x, camera.position.y + camera.viewportHeight / 2 - y)
        batch.end()
    }
}