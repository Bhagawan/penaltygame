package com.beavergames.footballgame.systems

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable
import com.beavergames.footballgame.assets.Assets
import com.beavergames.footballgame.components.AnimationComponent
import com.beavergames.footballgame.components.BallComponent
import com.beavergames.footballgame.components.DrawComponent
import com.beavergames.footballgame.components.TransformComponent
import com.beavergames.footballgame.util.Arrow
import com.beavergames.footballgame.util.PlayerPosition
import java.lang.Float.max
import kotlin.math.absoluteValue

class ControlSystem(private val world : World, private val camera: Camera, private val level : List<PlayerPosition>, priority : Int) : EntitySystem(priority) {
    private val terrainHeight = Gdx.graphics.height / 10.0f
    private val one = (Gdx.graphics.height - terrainHeight / 10.0f) / 10.0f
    private var fieldLength: Float = 0.0f
    private val obstacles = ArrayList<Body>()

    private lateinit var ballBody: Body
    private var arrow: Arrow? = null
    private var player: Entity? = null

    private var gameInterface: GameInterface? = null

    companion object {
        const val STATE_INTRO = 0
        const val STATE_AIM = 1
        const val STATE_SHOOT = 2
        const val STATE_PAUSE = 3
    }

    private var gameState = 0
    private var pausedState = 0
    private var introTimer = 120.0f

    private var ballsLeft = 5

    override fun addedToEngine(engine: Engine?) {
        initLevel()
    }

    override fun update(deltaTime: Float) {
        updateCamera(deltaTime)
        if(gameState == STATE_SHOOT) checkShoot()
    }


    ///// Public

    fun setInterface(i: GameInterface) {
        gameInterface = i
    }

    fun touch(x: Float, y: Float) {
        if(gameState == STATE_AIM) updateArrow(x, y)

    }

    fun touchUp(x: Float, y: Float) {
        when(gameState) {
            STATE_INTRO -> {
                camera.translate(camera.viewportWidth / 2.0f - camera.position.x, camera.viewportHeight / 2.0f - camera.position.y, 0.0f)
                gameState = STATE_AIM
            }
            STATE_AIM -> {
                updateArrow(x, y)
                ballBody.linearVelocity = arrow?.getPower()
                engine.removeEntity(arrow)
                gameState = STATE_SHOOT
                arrow = null
                player?.add(AnimationComponent(Assets.animKick))
            }
        }
    }

    fun pause(pause: Boolean) {
        if (pause) {
            pausedState = gameState
            gameState = STATE_PAUSE
        } else {
            gameState = pausedState
        }
    }

    fun restart() {
        ballsLeft = 5
        introTimer = 120.0f
        gameState = STATE_INTRO
        respawnBall()
        for(o in obstacles) {
            val e = o.userData
            if(e is Entity) engine.removeEntity(e)
            world.destroyBody(o)
        }
        createObstacles()
    }





    ///// Private

    private fun initLevel() {
        createTerrain()
        createLevel()
        createObstacles()
        createBall()
    }

    private fun createTerrain() {
        val bodyDef = BodyDef()
        bodyDef.position.set(0.0f, 0.0f)
        val terrainBody = world.createBody(bodyDef)

        val vertices = floatArrayOf(
            0.0f, 0.0f,
            0.0f, terrainHeight / one,
            Gdx.graphics.width * 3.0f / one, terrainHeight / one,
            Gdx.graphics.width * 3.0f / one, 0.0f,
            0.0f, 0.0f
        )
        val shape = PolygonShape()
        shape.set(vertices)
        terrainBody.createFixture(shape, 0.0f)
        shape.dispose()

        val entity = Entity()
        entity.add(TransformComponent(0.0f,0.0f,Gdx.graphics.width * 3.0f, terrainHeight))
        val t = Assets.assetManager.get("skin/skin.json", Skin::class.java).getRegion("terrain")
        val tT = TiledDrawable(t)
        tT.scale = (Gdx.graphics.height / 10.0f) / t.regionHeight
        entity.add(DrawComponent(tT))
        engine.addEntity(entity)
    }

    private fun createLevel() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val playerEntity = Entity()
        playerEntity.add(TransformComponent(0.0f, terrainHeight, one, one * 3))
        playerEntity.add(DrawComponent(Assets.animKick.getKeyFrame(0.0f)))
        engine.addEntity(playerEntity)
        player = playerEntity

        for(p in level) if(p.x < fieldLength) fieldLength = p.x
        fieldLength -= 4
        fieldLength *= -one
        fieldLength = max(Gdx.graphics.width * 1.5f, fieldLength + Gdx.graphics.width)

        val goalEntity = Entity()
        val goalTexture = skin.getRegion("football_gate")
        goalEntity.add(TransformComponent(fieldLength , 0.0f, goalTexture.regionWidth * goalTexture.regionHeight / (one * 5), (Gdx.graphics.height - terrainHeight) / 2.0f + terrainHeight))
        goalEntity.add(DrawComponent(goalTexture))
        engine.addEntity(goalEntity)


        val plankBodyDef = BodyDef()
        plankBodyDef.position.set(fieldLength / one + 1, terrainHeight / one + 5.0f)
        val plankBody = world.createBody(plankBodyDef)
        val plankShape = PolygonShape().apply { setAsBox(1.0f,0.1f) }
        plankBody.createFixture(plankShape, 0.0f)
        plankShape.dispose()
    }

    private fun createObstacles() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)

        for(player in level) {
            val bodyDef = BodyDef()
            val dX = when(player.angle) {
                -90.0f -> -1.5f
                else -> 0.5f
            }
            val dY = when(player.angle) {
                -90.0f -> 0.5f
                else -> 1.5f
            }
            bodyDef.position.set(fieldLength / one + player.x + dX, terrainHeight / one + player.y + dY)
            bodyDef.type = BodyDef.BodyType.DynamicBody

            val body = world.createBody(bodyDef)
            val fixDef = FixtureDef()
            val shape = createShape(player)
            fixDef.shape = shape
            fixDef.density = 0.1f
            fixDef.friction = 1.5f
            fixDef.restitution = 0.0f

            body.createFixture(fixDef)
            shape.dispose()

            val p = Entity()
            p.add(TransformComponent(fieldLength + player.x * one, terrainHeight + player.y * one, one * 1.2f, one * 3.35f).also { it.initialAngle = player.angle.absoluteValue })
            p.add(DrawComponent(skin.getRegion("player_two")))
            engine.addEntity(p)

            body.userData = p
            obstacles.add(body)
        }
    }

    private fun createShape(coord: PlayerPosition) : PolygonShape {
        val vertices = when(coord.angle) {
            -90.0f -> {
                floatArrayOf(
                    -1.5f, -0.5f,
                    -1.5f, 0.5f,
                    1.5f, 0.5f,
                    1.5f, -0.5f,
                    -1.5f, -0.5f
                )
            }
            0.0f -> {
                floatArrayOf(
                    -0.5f, -1.5f,
                    -0.5f, 1.5f,
                    0.5f, 1.5f,
                    0.5f, -1.5f,
                    -0.5f, -1.5f
                )
            }
            else -> { FloatArray(0) }
        }
        val pSh = PolygonShape()
        pSh.set(vertices)
        return pSh
    }

    private fun createBall() {
        val bodyDef = BodyDef()
        bodyDef.position.set(1.5f, terrainHeight / one + 1.5f)
        bodyDef.type = BodyDef.BodyType.DynamicBody
        ballBody = world.createBody(bodyDef)

        val shape = CircleShape()
        shape.radius = 0.5f
        val fixDef = FixtureDef()
        fixDef.shape = shape
        fixDef.density = 0.5f
        fixDef.friction = 0.5f
        fixDef.restitution = 0.5f

        ballBody.createFixture(fixDef)
        shape.dispose()

        ballBody.angularDamping = 3.5f

        val ball = Entity()
        ball.add(TransformComponent(one, terrainHeight + one, one,one))
        ball.add(DrawComponent(Assets.assetManager.get("skin/skin.json", Skin::class.java).getRegion("ball")))
        ball.add(BallComponent())
        engine.addEntity(ball)

        ballBody.userData = ball
    }

    private fun updateCamera(deltaTime: Float) {
        when(gameState) {
            STATE_INTRO -> {
                if(introTimer == 120.0f) camera.translate(fieldLength - Gdx.graphics.width / 2.0f + one - camera.position.x, Gdx.graphics.height / 2.0f - camera.position.y , 0.0f)
                else {
                    val step = (fieldLength - Gdx.graphics.width) / 2.0f * deltaTime.coerceAtMost(0.25f)
                    if((Gdx.graphics.width / 2 - camera.position.x).absoluteValue <= step) {
                        camera.translate(Gdx.graphics.width / 2 - camera.position.x, Gdx.graphics.height / 2 - camera.position.y, 0.0f)
                        gameState = STATE_AIM
                    } else camera.translate(-step, 0.0f, 0.0f)
                }
                introTimer -= deltaTime.coerceAtMost(0.25f)
            }
            else -> {
                val x = (ballBody.position.x * one).coerceAtLeast(Gdx.graphics.width / 2.0f).coerceAtMost(fieldLength - Gdx.graphics.width / 2.0f + one)
                val y = (ballBody.position.y * one).coerceAtLeast(Gdx.graphics.height / 2.0f)
                camera.translate(x - camera.position.x, y - camera.position.y, 0.0f)
            }
        }
        camera.update()
    }

    private fun updateArrow(x: Float, y: Float) {
        if(arrow == null) {
            arrow = Arrow(x, y)
            engine.addEntity(arrow)
        } else arrow?.setPointTo(x, y)
    }

    private fun checkShoot() {
        if((ballBody.position.x * one) >= fieldLength && (ballBody.position.y * one) in terrainHeight ..((Gdx.graphics.height - terrainHeight) / 2.0f + terrainHeight) ) {
            ballsLeft--
            gameInterface?.hit(true)
            if(ballsLeft > 0) {
                respawnBall()
                introTimer = 120.0f
                gameState = STATE_INTRO
            }
        } else if((ballBody.position.x * one) < -one || (ballBody.position.x * one) > fieldLength + one || (ballBody.linearVelocity.x == 0.0f && ballBody.linearVelocity.y == 0.0f)) {
            gameInterface?.hit(false)
            ballsLeft--
            if(ballsLeft > 0) {
                respawnBall()
                introTimer = 120.0f
                gameState = STATE_INTRO
            }
        }
    }

    private fun respawnBall() {
        if(ballBody.userData is Entity) engine.removeEntity(ballBody.userData as Entity)
        world.destroyBody(ballBody)
        createBall()
        player?.remove(AnimationComponent::class.java)
    }

    interface GameInterface {
        fun hit(goal: Boolean)
    }
}