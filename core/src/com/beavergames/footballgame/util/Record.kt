package com.beavergames.footballgame.util

data class Record(var levelNumber: Int, var record: Array<Boolean?>) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Record

        if (levelNumber != other.levelNumber) return false
        if (!record.contentEquals(other.record)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = levelNumber
        result = 31 * result + record.contentHashCode()
        return result
    }

    constructor() : this(0, emptyArray())
}
