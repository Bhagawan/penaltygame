package com.beavergames.footballgame.util

data class PlayerPosition(val x: Float, val y: Float, val angle: Float)
