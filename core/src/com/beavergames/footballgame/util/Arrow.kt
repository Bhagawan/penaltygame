package com.beavergames.footballgame.util

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.beavergames.footballgame.assets.Assets
import com.beavergames.footballgame.components.DrawComponent
import com.beavergames.footballgame.components.TransformComponent
import kotlin.math.acos
import kotlin.math.pow
import kotlin.math.sqrt

class Arrow(): Entity() {
    private val terrainHeight = Gdx.graphics.height / 10.0f
    private val one = (Gdx.graphics.height - terrainHeight / 10.0f) / 10.0f
    private val texture = Assets.assetManager.get("skin/skin.json", Skin::class.java).getRegion("shoot_arrow")
    private val tC = TransformComponent(one * 2, terrainHeight + one / 2.0f,0.0f, texture.regionHeight / 2.0f, one * 0.75f, texture.regionHeight.toFloat())
    private val forceVector = Vector2(0.0f, 0.0f)

    constructor(x: Float, y: Float) : this() {
        setPointTo(x, y)
    }

    init {
        add(DrawComponent(texture))
        add(tC)
    }

    fun setPointTo(x: Float, y: Float) {
        tC.width = sqrt((x - tC.pos.x).pow(2) + (y - tC.pos.y).pow(2))
        forceVector.x = (x - tC.pos.x) / one * 3.0f
        forceVector.y = (y - tC.pos.y) / one * 3.0f
        tC.angle =  Math.toDegrees(acos((x - tC.pos.x) / tC.width).toDouble()).toFloat().coerceAtLeast(0f).coerceAtMost(180f)
    }

    fun getAngle() :Float {
        return tC.angle
    }

    fun getPower() : Vector2 {
        return forceVector
    }
}