package com.beavergames.footballgame.util

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.beavergames.footballgame.assets.Assets

class LevelActor(level: Int, private var result: Array<Boolean?>?, private val w: Float, private val h: Float): Actor() {
    private val ribbon = Assets.assetManager.get("skin/skin.json", Skin::class.java).getSprite("ribbon")
    private val fail = Assets.assetManager.get("skin/skin.json", Skin::class.java).getSprite("fail")
    private val success = Assets.assetManager.get("skin/skin.json", Skin::class.java).getSprite("success")

    private val resultOffX = w / 538.0f * 59.0f
    private val ribbonHeight = ribbon.height * w / 538.0f
    private val rSize = (ribbonHeight / 114.0f * 65.0f) * 0.8f
    private val rStep = (w - resultOffX * 2.0f) / 6.0f

    private val flag = Assets.assetManager.get("skin/skin.json", Skin::class.java).getSprite(when(level) {
        0 -> "ru"
        1 -> "au"
        2 -> "nl"
        3 -> "jp"
        4 -> "pt"
        5 -> "eng"
        6 -> "br"
        7 -> "mr"
        8 -> "chr"
        9 -> "fr"
        else -> "ar"
    })

    override fun draw(batch: Batch?, parentAlpha: Float) {

        batch?.draw(flag, x + resultOffX, y + rSize, w - resultOffX * 2, (w - resultOffX * 2) / (80.0f / 50.0f))
        batch?.draw(ribbon, x, y, w, ribbonHeight)

        result?.let {
            for(n in it.indices) {
                when(it[n]) {
                    true -> batch?.draw(success, x + resultOffX + (n + 1) * rStep - rSize / 2.0f, y + ribbonHeight / 114.0f * 43 - rSize / 2, rSize, rSize)
                    else -> batch?.draw(fail, x + resultOffX + (n + 1) * rStep - rSize / 2.0f, y + ribbonHeight / 114.0f * 43 - rSize / 2, rSize, rSize)
                }
            }
        }
    }
}