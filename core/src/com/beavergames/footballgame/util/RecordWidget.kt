package com.beavergames.footballgame.util

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.beavergames.footballgame.assets.Assets

class RecordWidget(private var result: Array<Boolean?>): Actor() {
    private val ball = Assets.assetManager.get("skin/skin.json", Skin::class.java).getSprite("ball")
    private val fail = Assets.assetManager.get("skin/skin.json", Skin::class.java).getSprite("fail")
    private val success = Assets.assetManager.get("skin/skin.json", Skin::class.java).getSprite("success")

    private var v = true

    override fun draw(batch: Batch?, parentAlpha: Float) {
        if(v) {
            val space = width / result.size / 5
            val w = (width - space * (result.size - 1)) / result.size
            for(n in result.indices) {
                when(result[n]) {
                    true -> batch?.draw(success, x + n * (w + space) + w / 4.0f, y + height / 2.0f - w / 4.0f, w / 2.0f, w / 2.0f)
                    false -> batch?.draw(fail, x + n * (w + space) + w / 4.0f, y + height / 2 - w / 4.0f, w / 2.0f, w / 2.0f)
                    else -> batch?.draw(ball, x + n * (w + space), y , w, w)
                }
            }
        }
    }

    fun setResult(result: Array<Boolean?>) {
        this.result = result
    }

    fun show() {
        v = true
    }

    fun hide() {
        v = false
    }


}