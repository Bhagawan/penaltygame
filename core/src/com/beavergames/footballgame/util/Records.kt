package com.beavergames.footballgame.util

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Json
import com.beavergames.footballgame.assets.Levels

class Records {
    companion object {
        fun saveRecord(level: List<PlayerPosition>, result: Array<Boolean?>) {
            val prefs = Gdx.app.getPreferences("Records")
            val j = Json()
            var savedRecords = j.fromJson(Array<Record>::class.java, prefs.getString("records", "[]"))

            val n = Levels.getLevels().indexOf(level)
            var newResult = 0
            result.forEach { if(it == true) newResult++ }

            var index = -1
            for(r in savedRecords) {
                if(r.levelNumber == n) {
                    index = savedRecords.indexOf(r)
                    break
                }
            }
            if(index >= 0) {
                var old = 0
                savedRecords[index].record.forEach { if(it == true) old++ }
                if(old <= newResult) savedRecords[index] = Record(n, result)
            } else savedRecords = savedRecords.plusElement(Record(n, result))

            prefs.putString("records", j.toJson(savedRecords))
            prefs.flush()
        }

        fun getRecords() : Array<Record> {
            val prefs = Gdx.app.getPreferences("Records")
            val r = prefs.getString("records", "[]")
            return Json().fromJson(Array<Record>::class.java, r)
        }
    }
}