package com.beavergames.footballgame

import com.badlogic.gdx.Game
import com.beavergames.footballgame.assets.Assets
import com.beavergames.footballgame.screens.MenuScreen

class FootballGame : Game() {

	companion object {
		const val PERFECT_DT: Float = (16.67 * 2 / 1000).toFloat()
	}

	override fun create () {
		Assets.init()
		setScreen(MenuScreen(this))
	}

	override fun dispose () {
		Assets.dispose()
	}
}
