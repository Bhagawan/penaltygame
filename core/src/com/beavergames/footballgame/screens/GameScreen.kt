package com.beavergames.footballgame.screens

import com.badlogic.ashley.core.Engine
import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.ScreenUtils
import com.beavergames.footballgame.systems.*
import com.beavergames.footballgame.util.PlayerPosition
import com.beavergames.footballgame.util.Records

class GameScreen(private val game: Game, private val level: List<PlayerPosition>): ScreenAdapter() {
    private var batch = SpriteBatch()
    private val camera = OrthographicCamera()
    private val engine = Engine()
    private val world = World(Vector2(0.0f, -10.0f), true)
    private val debugRenderer = Box2DDebugRenderer()


    private var frameCounter = 0.0f
    private var physicsCamera = false

    private val controlSystem = ControlSystem(world,camera, level, 1)
    private val uiSystem = UISystem(level,110).apply {
        setInterface(object : UISystem.UIInterface {
            override fun pause(pause: Boolean) {
                controlSystem.pause(pause)
            }

            override fun exit() {
                game.screen = LevelChoiceScreen(game)
            }

            override fun restart() {
                controlSystem.restart()
            }

            override fun nextLevel(level: List<PlayerPosition>) {
                game.screen = GameScreen(game, level)
            }

            override fun saveRecord(result: Array<Boolean?>) {
                Records.saveRecord(level, result)
            }

        })
    }

    init {
        camera.setToOrtho(false)


        controlSystem.setInterface(object : ControlSystem.GameInterface {
            override fun hit(goal: Boolean) {
                uiSystem.hit(goal)
            }
        })
        engine.addSystem(uiSystem)

        engine.addSystem(controlSystem)
        engine.addSystem(BodyTextureSystem(world, 2))
        engine.addSystem(BackgroundSystem(camera, 100))
        engine.addSystem(RenderSystem(batch, camera,101))
    }

    override fun show() {
        val inputAdapter = object : InputAdapter() {

            override fun keyDown(keycode: Int): Boolean = when(keycode) {
                Input.Keys.TAB -> {
                    game.screen = MenuScreen(game)
                    true
                }
                Input.Keys.SPACE -> {
                    if (physicsCamera) {
                        camera.setToOrtho(false, 20.0f,11.0f)
                        batch.projectionMatrix = camera.combined
                        physicsCamera = false
                    } else {

                        camera.setToOrtho(false)
                        batch.projectionMatrix = camera.combined
                        physicsCamera = true
                    }
                    true
                }
                Input.Keys.ENTER -> {
                    true
                }
                Input.Keys.A -> {
                    camera.translate(-10.0f, 0.0f, 0.0f)
                    true
                }
                Input.Keys.D -> {
                    camera.translate(10.0f, 0.0f, 0.0f)
                    true
                }
                Input.Keys.W -> {
                    camera.translate(0.0f, 10.0f, 0.0f)
                    true
                }
                Input.Keys.S -> {
                    camera.translate(0.0f, -10.0f, 0.0f)
                    true
                }
                else -> false
            }

            override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
                val x = (screenX - Gdx.graphics.width / 2.0f) / (Gdx.graphics.width / camera.viewportWidth) + camera.position.x
                val y = camera.position.y - (screenY - Gdx.graphics.height / 2.0f) / (Gdx.graphics.height / camera.viewportHeight)
                controlSystem.touch(x, y)
                return true
            }

            override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
                val x = (screenX - Gdx.graphics.width / 2.0f) / (Gdx.graphics.width / camera.viewportWidth) + camera.position.x
                val y = camera.position.y - (screenY - Gdx.graphics.height / 2.0f) / (Gdx.graphics.height / camera.viewportHeight)
                controlSystem.touch(x, y)
                return true
            }

            override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
                val x = (screenX - Gdx.graphics.width / 2.0f) / (Gdx.graphics.width / camera.viewportWidth) + camera.position.x
                val y = camera.position.y - (screenY - Gdx.graphics.height / 2.0f) / (Gdx.graphics.height / camera.viewportHeight)
                controlSystem.touchUp(x, y)
                return true
            }
        }
        val inputMultiplexer = InputMultiplexer()
        inputMultiplexer.addProcessor(uiSystem.getStage())
        inputMultiplexer.addProcessor(inputAdapter)
        Gdx.input.inputProcessor = inputMultiplexer
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)

        val frameTime = delta.coerceAtMost(0.25f)
        frameCounter += frameTime
        while (frameCounter >= 1.0f/60) {
            world.step(1.0f/60, 6, 6)
            frameCounter -= 1.0f/60
        }
        debugRenderer.render(world, camera.combined)

        engine.update(delta)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
    }
}