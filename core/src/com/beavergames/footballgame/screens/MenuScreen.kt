package com.beavergames.footballgame.screens

import com.badlogic.gdx.*
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.ScreenUtils
import com.beavergames.footballgame.FootballGame
import com.beavergames.footballgame.assets.Assets

class MenuScreen(private val game: Game): ScreenAdapter() {
    private val stage = Stage()

    init {
        createMenu()
    }

    override fun show() {
        val inputAdapter = object : InputAdapter() {

            override fun keyDown(keycode: Int): Boolean = when(keycode) {
                Input.Keys.TAB -> {
                    game.screen = LevelChoiceScreen(game)
                    true
                }
                else -> false
            }
        }
        val inputMultiplexer = InputMultiplexer()
        inputMultiplexer.addProcessor(inputAdapter)
        inputMultiplexer.addProcessor(stage)
        Gdx.input.inputProcessor = inputMultiplexer
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)
        stage.act(Gdx.graphics.deltaTime.coerceAtMost(FootballGame.PERFECT_DT))
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
        stage.clear()
    }

    override fun dispose() {
        stage.dispose()
    }

    private fun createMenu() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val table = Table()
        table.setFillParent(true)
        table.background = TiledDrawable(skin.getRegion("seatTile"))
        stage.addActor(table)

        table.pad(20.0f)

        val btnWidth = Gdx.graphics.width / 3.0f

        table.add(Image(skin.getSprite("title").apply { setSize(stage.width / 3 * 2.0f, stage.height / 3.0f) }))
            .center()
            .size(stage.width / 3.0f * 2.0f, stage.height / 3.0f)
            .padTop(stage.height / 10.0f)
        table.row()

        val btnOff = btnWidth / 2 - (stage.width / 3.0f / 1830.0f * 373)

        val newGameButton = TextButton("Новая Игра", skin).apply {
            addListener(object : ClickListener() {
                override fun touchUp(
                    event: InputEvent?,
                    x: Float,
                    y: Float,
                    pointer: Int,
                    button: Int
                ) {
                    game.screen = LevelChoiceScreen(game)
                }
            })
            label.setAlignment(Align.left)
        }
        table.center().add(newGameButton).size(btnWidth, btnWidth / 4.0f).expand().center().padLeft(btnOff)
        table.row()

        val exitButton = TextButton("Выход",skin).apply { addListener( object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    Gdx.app.exit()
                }
            })
            label.setAlignment(Align.left)
        }
        table.center().add(exitButton).size(btnWidth, btnWidth / 4.0f).expand().center().padBottom(50.0f).padLeft(btnOff)
        table.row()
    }
}