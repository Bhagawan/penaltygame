package com.beavergames.footballgame.screens

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable
import com.badlogic.gdx.utils.ScreenUtils
import com.beavergames.footballgame.FootballGame
import com.beavergames.footballgame.assets.Assets
import com.beavergames.footballgame.assets.Levels
import com.beavergames.footballgame.util.LevelActor
import com.beavergames.footballgame.util.Records

class LevelChoiceScreen(private val game: Game): ScreenAdapter() {
    private val stage = Stage()

    init {
        createMenu()
    }

    override fun show() {
        Gdx.input.inputProcessor = stage
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)
        stage.act(Gdx.graphics.deltaTime.coerceAtMost(FootballGame.PERFECT_DT))
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
        stage.clear()
    }

    override fun dispose() {
        stage.dispose()
    }

    private fun createMenu() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val table = Table()
        table.background = TiledDrawable(skin.getRegion("seatTile"))
        table.setFillParent(true)
        stage.addActor(table)
        table.pad(10.0f)

        val size = stage.width / 9.0f

        val backButton = ImageButton(SpriteDrawable(skin.getSprite("back_btn").apply { setSize(size, size) })).apply { addListener( object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    game.screen = MenuScreen(game)
                }
            })
        }
        table.top().add(backButton).padLeft(stage.width / 4.0f - size - (stage.width / 4.0f) * 0.3f).padTop(stage.height / 8).top().right()

        table.top().add(Container(Label("Выберите матч", skin, "recordsHeader")).apply {
            isTransform = true
            scaleBy(0.3f) }).center().expandX()
        table.add().width(stage.width / 4.0f * 1.3f)
        table.row()

        val recordsGroup = VerticalGroup()
        val scroll = ScrollPane(recordsGroup)
        scroll.setFlickScroll(true)
        recordsGroup.space(10.0f)

        table.add(scroll).top().expand().fill().colspan(3).padTop(10.0f)

        val records = Records.getRecords()
        val levels = Levels.getLevels()
        for(n in levels.indices step 2) {
            val recLineTable = Table()
            val width = stage.width / 4.0f * 0.9f
            var record : Array<Boolean?>? = null
            for (r in records) {
                if(r.levelNumber == n) {
                    record = r.record
                    break
                }
            }
            recLineTable.add(LevelActor(n, record, width, width * 0.75f).apply { addListener ( object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    game.screen = GameScreen(game, levels[n])
                }
            }) }).expand()
                .padRight(stage.width / 4.0f * 0.1f)
                .minHeight(width * 0.75f)
                .minWidth(width)

            if(n + 1 < levels.size) {
                record = null
                for (r in records) {
                    if(r.levelNumber == n + 1) {
                        record = r.record
                        break
                    }
                }
                recLineTable.add(LevelActor(n + 1, record, width, width * 0.75f).apply { addListener ( object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        game.screen = GameScreen(game, levels[n + 1])
                    }
                }) }).expand().padRight(stage.width / 4.0f * 0.1f)
                    .minHeight(width * 0.75f)
                    .minWidth(width)
            }
            recordsGroup.addActor(recLineTable)
        }
    }
}