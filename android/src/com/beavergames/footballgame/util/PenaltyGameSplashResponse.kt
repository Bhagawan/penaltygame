package com.beavergames.footballgame.util

import androidx.annotation.Keep

@Keep
data class PenaltyGameSplashResponse(val url : String)